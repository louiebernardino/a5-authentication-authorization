// members - routes 
//Declare dependencies and model
const Member = require("../models/members");
const express = require("express");
const router = express.Router(); //to handle routing
const bcrypt = require("bcryptjs")
const auth = require("../middleware/auth")

//1) CREATE
router.post("/", auth, async (req, res) => {
	const member = new Member(req.body);
	//Commented out for async operation//
	// member.save()
	// 	.then(() => {
	// 		res.send(member)
	// 	})
	// 	.catch((e) => {
	// 		res.status(400).send(e)
	// 	})
	//Commented out for async operation//
	try {
		await member.save(); 
		res.status(201).send(member);
	} catch(e) {
		res.status(400).send(e.message);
	}
});

//2) GET ALL
router.get("/", async (req, res) => {
	//Commented out for async operation//
	// Member.find()
	// 	.then((members) => {
	// 		return res.status(200).send(members)
	// 	})
	// 	.catch((e) => {
	// 		return res.status(500).send(e)
	// 	})
	//Commented out for async operation//
	try {
		const members = await Member.find();
		return res.status(200).send(members); //if no return, it will execute the succeeding lines in the command line
	} catch(e) {
		return res.status(404).send(e.message)
	}
});

//3) GET ONE
router.get("/:id", async (req, res) => {
	const _id = req.params.id;
	//Commented out for async operation//
	// Member.findById(_id)
	// 	.then((member) => {
	// 		if(!member) {
	// 			return res.status(404).send(e)
	// 		}

	// 		return res.send(member)
	// 	})
	// 	.catch((e) => {
	// 		return res.status(500).send(e)
	// 	})
	//Commented out for async operation//
	try {
		const member = await Member.findById(_id);
		if(!member) {
			return res.status(404).send("Member doesn't exist!");
		}
		res.send(member)
	} catch(e) {
		return res.status(500).send(e);
	}
});

//4) UPDATE ONE
router.patch("/:id", async (req, res) => {
	const _id = req.params.id;
	//Commented out for async operation//
	// Member.findByIdAndUpdate(_id, req.body, { new: true })
	// 	.then((member) => {
	// 		if(!member) {
	// 			return res.status(404).send(e)
	// 		}

	// 		return res.send(member)
	// 	})
	// 	.catch((e) => {
	// 		return res.status(500).send(e)
	// 	})
	//Commented out for async operation//
	
	//FUNCTIONALITY TO LIMIT ALLOWED UPDATES
	const updates = Object.keys(req.body);
	// console.log(updates);

	const allowedUpdates = ["firstName", "lastName", "position", "password"];

	const isValidUpdate = updates.every(update => allowedUpdates.includes(update))

	if(!isValidUpdate) {
		return res.status(400).send({ error: "Invalid update"});
	}
	//end of function

	try {
		// const member = await Member.findByIdAndUpdate(_id, req.body, { new: true });

		const member = await Member.findById(_id);
		updates.map(update => member[update] = req.body[update])

		if(!member) {
			return res.status(404).send("Member doesn't exist!");
		}
		await member.save();
		res.send(member);
	} catch(e) {
		return res.status(500).send(e);
	}
});

//5) DELETE ONE
router.delete("/:id", async (req, res) => {
	const _id = req.params.id;
	//Commented out for async operation//
	// Member.findByIdAndDelete(_id)
	// 	.then((member) => {
	// 		if(!member) {
	// 			return res.status(404).send(e)
	// 		}

	// 		return res.send(member)
	// 	})
	// 	.catch((e) => {
	// 		return res.status(500).send(e)
	// 	})
	//Commented out for async operation//
	try {
		const member = await Member.findByIdAndDelete(_id);
		if(!member) {
			return res.status(404).send("Member doesn't exist");
		}
		res.send(member);
	} catch(e) {
		return res.status(500).send(e);
	}
});

//6) LOGIN
router.post("/login", async(req, res) => {
	try	{
		//submit email and password
		const member = await Member.findOne({email: req.body.email})
		const members = await Member.findOne({username: req.body.username})
		if(!member) {
			return res.send({"message": "Invalid Login Credentials!"})
		}

		const isMatch = await bcrypt.compare(req.body.password, member.password)

		if(!isMatch) {
			return res.send({ "message": "Invalid Login Credentials!"})
		}

		//generate token
		const token = await member.generateAuthToken()
		res.send({member, token})
	} catch(e) {
		res.status(500).send(e)
	}
})

//7) GET LOGIN USER'S PROFILE

//8) LOGOUT

module.exports = router;