const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const uniqueValidator = require("mongoose-unique-validator");
const validator = require("validator");
const bcrypt = require('bcryptjs')
const jwt = require("jsonwebtoken")

//Define your schema
const memberSchema = new Schema(
	{
		firstName: {
			type: String,
			trim: true,
			maxlength: 30,
			default: "J"
		},

		lastName: {
			type: String,
			trim: true,
			maxlength: 30,
			default: "Doe"
		},
		username: {
			type: String,
			required: true,
			trim: true,
			maxlength: 10,
			index: true,
			unique: true,
			validate(value) {
				if(!validator.isAlphanumeric(value)) {
					throw new Error("No special characters!")
				}
				console.log("Username has been added")
			}
		},
		position: {
			type: String,
			enum: ["instructor", "student", "hr", "admin", "ca" ],
			required: true
		},
		age: {
			type: Number,
			min: 18,
			required: true
		},
		email:
		{
			type: String,
			required: true,
			trim: true,
			unique: true,
			validate(value) {
				if(!validator.isEmail(value)) {
					throw new Error("Email is invalid!")
				}
			}
		},
		password:
		{
			type: String,
			required: true,
			minlength: [5, "Password must have at least 5 characters!"]
		},
		tokens: [
			{
				token: {
					type: String,
					required: true
				}
			}
		]
		
	},
	{
		timestamps: true
	}

);

//HASH PASSWORD WHEN UPDATING AND CREATING MEMBER
memberSchema.pre("save", async function(next) { 
	//we will use function because arrow function ()=> does not bind this
	const member = this

	if(member.isModified('password')) {
		//salt
		const salt = await bcrypt.genSalt(10);
		//hash
		member.password = await bcrypt.hash(member.password, salt);
	}

	next();
})

//find by credentials using statics
memberSchema.statics.findByCredentials = async(email, password) => {
	//check if email exists
	const member = await Member.findOne({ email })

	if(!member) {
		throw new Error("Email is wrong!") //for now
	}

	//comapring req pw  vs unhashed db password
	const isMatch = await bcrypt.compare(password, member.password)

	if(!isMatch) {
		throw new Error("Wrong Password!")
	}

	return member
}

//Generate Token
memberSchema.methods.generateAuthToken = async function() {
	const member = this

	//1 - data to embed
	//2 - secret message
	//3 - option
	const token = jwt.sign({_id: member._id.toString()}, "batch40", {expiresIn: "2 days"})
	member.tokens = member.tokens.concat({token})
	await member.save()
	return token

	//save the token to our db
}

memberSchema.plugin(uniqueValidator)
//export your model
const Member = mongoose.model("Member", memberSchema)
module.exports = Member
